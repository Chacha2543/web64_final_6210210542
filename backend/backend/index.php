<?php 
    include("connect_db.php");
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
        crossorigin="anonymous">
    <title>index page</title>
</head>

<body>
    <section>
        <div class="container">
            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                            $sql = "SELECT * FROM tb_contact";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<tr>";
                                    echo "<th>".$row['contact_name']."</th>";
                                    echo "<th>".$row['contact_username']."</th>";
                                    echo "<th>".$row['contact_email']."</th>";
                                    echo "<th><a href='".$_SERVER['PHP_SELF']."?id=".$row['contact_id']."'>edit</th>";
                                    echo "<th><a href='del_contact.php?id=".$row['contact_id']."'>delete</th>";
                                    
                                    echo "</tr>";
                                }
                            } else {
                                echo "<tr>";
                                echo "<th colspan='3'>No data available</th>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            
            <?php
                if(isset($_GET['id'])){
                    $sql    = "SELECT * FROM tb_contact WHERE contact_id = '".$_GET['id']."'";
                    $result = $conn->query($sql);
                    $row    = $result->fetch_assoc();

            ?>
                <form action="edit_contact.php" method="POST" >
                    <input type="hidden" name="contact_id" value="<?php echo $row['contact_id']; ?>" >
                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="Name" value="<?php echo $row['contact_name']; ?>" class="form-control"  placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label >Username</label>
                        <input type="text" name="Username" value="<?php echo $row['contact_username']; ?>" class="form-control"  placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label >Email</label>
                        <input type="email" name="Email" value="<?php echo $row['contact_email']; ?>" class="form-control"  placeholder="Email">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            <?php 
                } else { 
            ?>

                <form action="add_contact.php" method="POST" >
                    <div class="form-group">
                        <label >Name</label>
                        <input type="text" name="Name" value="" class="form-control"  placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label >Username</label>
                        <input type="text" name="Username" value="" class="form-control"  placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label >Email</label>
                        <input type="email" name="Email" value="" class="form-control"  placeholder="Email">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            <?php 
                }  
            ?>
        </div>
    </section>
</body>

</html>
