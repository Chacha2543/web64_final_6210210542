<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Firebase\JWT\JWT ; 
$router->get('/', function () use ($router) {
    return $router->app->version();
});
/*$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$result = new stdClass();
    $result->username = $request->input("username");
    $result->password = $request->input("password");

    return response()->json($result);
});*/
$router->post('/register', function(Illuminate\Http\Request $request) {
	
	
    $name = $request->input("name");
    $email = $request->input("email");
    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));
    
    $query = app('db')->insert('INSERT into USER
    (Name, Email, Username, Password) 
    VALUE(?,?,?,?)',
    [$name,$email,$username,$password]);

    return "Ok";
});
$router->post('/login', function(\Illuminate\Http\Request $request){
    $username = $request->input("username");
    $password = $request->input("password");

    $result = app('db')->select('SELECT password from user WHERE Username=?',[$username]);
    $loginResult = new stdClass();
    
    if(count($result) == 0){
        $loginResult->status = "success";
        $loginResult->reason = "User is not founded";
    }else{
        if(app('hash')->check($password, $result[0]->password)){
            $loginResult->status = "success";
            
            $payload = [
                'iss' =>"sign in-out ",
                'sub' => $result[0]->UserID,
                'iat' => time(),
                'exp' => time() + 30 * 60 * 60,
            ];

            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
            return response()->json($loginResult);
        }else{
            $loginResult->status = "fail";
            $loginResult->reason = "Incorract Password";
            return response()->json($loginResult);
        }
    }
    return response()->json($loginResult);
});
